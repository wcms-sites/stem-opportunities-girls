core = 7.x
api = 2


/**
 * Custom Modules
 */

; UW Configuration stem for girls site
projects[uw_cfg_stem_for_girls][type] = "module"
projects[uw_cfg_stem_for_girls][download][type] = "git"
projects[uw_cfg_stem_for_girls][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_stem_for_girls.git"
projects[uw_cfg_stem_for_girls][download][tag] = "7.x-1.0"
projects[uw_cfg_stem_for_girls][subdir] = "features"
